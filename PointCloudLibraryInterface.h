// Objective-C++ objective-c class to interface with C++ Library
#import <Foundation/Foundation.h>

// typedef struct {
//     float x;
//     float y;
//     float z;
//     float r;
//     float g;
//     float b;
//     float a;
// } Vertex;
// Swift/Object-C と点群データをやり取りする際の構造体を定義する
// struct SwiftPointXYZRGBA{
//     float x;
//     float y;
//     float z;
//     float r;
//     float g;
//     float b;
//     float a;
// };
// 
// typedef struct SwiftPointXYZRGBA SwiftPointXYZRGBA;

@interface PointCloudLibraryInterface : NSObject

@property (nonatomic, copy) NSArray *pointArray;

- (void)callLoad:(NSString *)argString;
- (void)callFiltering;
// - (void)setPointCloudData:(NSArray<NSValue *>)pointArray;
- (NSArray<NSValue *>)GetPointCloudData;
@end
